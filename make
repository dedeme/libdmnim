#!/bin/bash

case $1 in
c*)
  nim c -p:src/dmnim -o:bin/main tests/main.nim 2>&1 | grep -v '^Hint:'
  exit 0
  ;;
x*)
  nim c -p:src/dmnim -o:bin/main tests/main.nim 2>&1 | grep -v '^Hint:'
  bin/main
  exit 0
  ;;
*)
  echo $1: Unknown option
  ;;
esac

