from cryp import nil

proc crypTest*() =
  echo "cryp test:"

  var b64 = cryp.genk(6)
  doAssert(len(b64) == 6)

  b64 = cryp.key("deme", 6)
  doAssert(b64 == "wiWTB9")
  b64 = cryp.key("Generaro", 5)
  doAssert(b64 == "Ixy8I")
  b64 = cryp.key("Generara", 5)
  doAssert(b64 == "0DIih")

  b64 = cryp.cryp("abc", "01")
  var s = cryp.decryp("abc", b64)
  doAssert(s == "01")
  b64 = cryp.cryp("abcd", "11")
  s = cryp.decryp("abcd", b64)
  doAssert(s == "11")
  b64 = cryp.cryp("abc", "")
  s = cryp.decryp("abc", b64)
  doAssert(s == "")
  b64 = cryp.cryp("c", "a")
  s = cryp.decryp("c", b64)
  doAssert(s == "a")
  b64 = cryp.cryp("xxx", "ab c")
  s = cryp.decryp("xxx", b64)
  doAssert(s == "ab c")
  b64 = cryp.cryp("abc", "\n\ta€b c")
  s = cryp.decryp("abc", b64)
  doAssert(s == "\n\ta€b c")

  echo "    Finished"
