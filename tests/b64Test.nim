from b64 import nil

proc b64Test*() =
  echo "b64 test:"

  doAssert(b64.encode("Cañónç䍆") == "Q2HDscOzbsOn5I2G")
  doAssert(b64.encode("") == "")
  doAssert(b64.decode(b64.encode("Cañónç䍆")) == "Cañónç䍆")

  var bss: seq[uint8] = @[]
  for i in 0.uint8..<130.uint8:
    bss.add(i)

  var bss2 = b64.decodeBytes(b64.encodeBytes(bss))
  for i in 0..<130:
    doAssert(bss[i] == bss2[i])

  echo "    Finished"
