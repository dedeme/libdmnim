from sys import nil
import strutils

proc sysTest*() =
  echo "Sys test:"

  doAssert sys.home().endsWith("dmNimTest")

  echo "    Finished"
