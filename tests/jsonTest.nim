import json
import strutils

proc jsonTest*() =
  echo "json test:"

  doAssert "null".parseJson.kind == JNull
  doAssert "  null  ".parseJson.kind == Jnull

  doAssert " true ".parseJson.getBool
  doAssert (not " false".parseJson.getBool)

  try:
    doAssert " xxx".parseJson.getBool
    quit "Not reachable in try 1"
  except JsonParsingError:
    doAssert getCurrentExceptionMsg().startsWith("input")

  doAssert (% true).getBool
  doAssert (not (% false).getBool)

  doAssert " 0 ".parseJson.getInt == 0
  doAssert " 12 ".parseJson.getInt == 12
  doAssert " 0.23 ".parseJson.getFloat == 0.23
  doAssert " -0 ".parseJson.getInt == 0
  doAssert " -0.0 ".parseJson.getFloat == 0
  doAssert " -12.11 ".parseJson.getFloat == -12.11
  doAssert " -12.11e22 ".parseJson.getFloat == -12.11e22

  try:
    doAssert " 12abc ".parseJson.getInt == 12
    quit "Not reachable in try 2"
  except JsonParsingError:
    doAssert getCurrentExceptionMsg().startsWith("input")

  doAssert "  \"\" ".parseJson.getStr == ""
  doAssert "  \"a\\u0030\" ".parseJson.getStr == "a0"
  doAssert "  \"a\\t\\n\\\"\" ".parseJson.getStr == "a\t\n\""

  doAssert "[]".parseJson.getElems.len == 0
  var js = "[123]".parseJson
  doAssert js[0].getInt == 123
  js = " [-123.56, true, [],  [\"azf\", false], 56]  ".parseJson
  doAssert js[0].getFloat == -123.56
  doAssert js[1].getBool
  var js1 = js[2]
  doAssert js1.getElems.len == 0
  js1 = js[3]
  doAssert js1[0].getStr == "azf"
  doAssert (not js1[1].getBool)
  doAssert js[4].getInt == 56

  doAssert 0 == (%* []).getElems.len
  doAssert 1 == (%* [1])[0].getInt
  js = %* [-16, 1.0, "caf", "a\n\tzzð"]
  doAssert -16 == js[0].getInt
  doAssert 1 == js[1].getFloat
  doAssert "caf" == js[2].getStr
  doAssert "a\n\tzzð" == js[3].getStr

  js = " {\"a\":true, \"a\":123, \"b\": {\"1\": \"one\"} } ".parseJson
  doAssert 123 == js["a"].getInt
  js1 = js["b"]
  doAssert "one" == js1["1"].getStr
  doAssert ($ js) == "{\"a\":123,\"b\":{\"1\":\"one\"}}"

  echo "    Finished"
