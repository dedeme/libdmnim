# Copyright 19-Sep-2018 ºDeme
# GNU General Public License - V3 <http://www.gnu.org/licenses/>

from os import nil

proc exists*(file: string): bool =
  ## Returns true if the file exists.
  return os.existsFile(file) or os.existsDir(file)

proc isDirectory*(dir: string): bool =
  ## Returns true if the file exists and is a directory.
  return os.existsDir(dir)

proc cd*(dir: string) =
  ## Changes the working directory
  os.setCurrentDir(dir)

proc cwd*(): string =
  ## Returns the working directory
  return os.getCurrentDir()

proc mkdir*(dir: string) =
  ## If function fails, `OSError` is raised. It does **not** fail
  ## if the directory already exists.
  os.createDir(dir)
