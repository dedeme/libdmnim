# Copyright 19-Sep-2018 ºDeme
# GNU General Public License - V3 <http://www.gnu.org/licenses/>

from ospaths import nil
from random import nil
from file import nil

var homeDir: string = nil

proc init*(home: string) =
  ## Initialize a program
  homeDir = ospaths.getHomeDir() & ".dmNimApp/" & home
  file.mkdir homeDir
  random.randomize()

proc home*(): string =
  ## Returns the application working directory
  if homeDir == nil:
    raise newException(NilAccessError, "sys.init has not been called")
  return homeDir
