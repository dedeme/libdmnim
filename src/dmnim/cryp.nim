# Copyright 20-Sep-2018 ºDeme
# GNU General Public License - V3 <http://www.gnu.org/licenses/>

from random import nil
from b64 import nil

const b64Base =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

proc genk*(lg: uint): string =
  ## Generates a random key of 'lg' characters

  result = newString(lg)
  for i in 0..<cast[int](lg):
    result[i] = b64Base[random.rand(64)]

proc key*(key: string, klg: int): string =
  ## Returns 'key' codified irreversibly in a string of 'klg' characters
  if key == "":
    raise newException(ValueError, "key is empty")
  var lg = klg
  if lg < 1: lg = 1

  var k0 = key & "codified in irreversibleDeme is good, very good!\n\r8@@"

  var k = b64.decodeBytes(b64.encode(k0))
  var lenk = len(k)
  var sum = 0.uint8
  for i in 0..<lenk:
      sum += k[i]

  var lg2 = lg + lenk
  var ra: seq[uint8]
  newSeq(ra, lg2)
  var ra1: seq[uint8]
  newSeq(ra1, lg2)
  var ra2: seq[uint8]
  newSeq(ra2, lg2)

  var ik = 0
  for i in 0..<lg2:
    var v1 = cast[int](k[ik])
    var v2 = v1 + cast[int](k[v1 mod lenk])
    var v3 = v2 + cast[int](k[v2 mod lenk])
    var v4 = v3 + cast[int](k[v3 mod lenk])
    sum += cast[uint8](i + v4)
    ra1[i] = sum
    ra2[i] = sum
    ik += 1
    if ik == lenk: ik = 0

  for i in 0..<lg2:
    var v1 = cast[int](ra2[i])
    var v2 = v1 + cast[int](ra2[v1 mod lg2])
    var v3 = v2 + cast[int](ra2[v2 mod lg2])
    var v4 = v3 + cast[int](ra2[v3 mod lg2])
    sum += cast[uint8](v4)
    ra2[i] = sum
    ra[i] = sum + ra1[i]

  return b64.encodeBytes(ra)[0..<lg]

proc cryp*(k: string, c: string): string =
  ## Codifies 'c' with key 's'

  if k == "":
    raise newException(ValueError, "key is empty")

  var b64s = cast[seq[uint8]](b64.encode(c))
  var lg = len(b64s)
  var k2 = cast[seq[uint8]](key(k, lg))
  var bs: seq[uint8]
  newSeq(bs, lg)
  for i in 0..<lg:
    bs[i] = b64s[i] + k2[i]

  return b64.encodeBytes(bs)

proc decryp*(k: string, c: string): string =
  ## Restores 'c' with key 's'

  if k == "":
    raise newException(ValueError, "key is empty")

  var bs = b64.decodeBytes(c)
  var lg = len(bs)
  var k2 = cast[seq[uint8]](key(k, lg))
  var b64s: seq[uint8]
  newSeq(b64s, lg)
  for i in 0..<lg:
    b64s[i] = bs[i] - k2[i]

  return b64.decode(cast[string](b64s))

