# Copyright 19-Sep-2018 ºDeme
# GNU General Public License - V3 <http://www.gnu.org/licenses/>

## Encode and decode in base64

from base64 as b64 import nil

proc encode*(s: string): string =
  return b64.encode(s, newLine = "")

proc decode*(s: string): string =
  return b64.decode(s)

proc encodeBytes*(s: seq[uint8]): string =
  return b64.encode(s, newLine = "")

proc decodeBytes*(s: string): seq[uint8] =
  return cast[seq[uint8]](b64.decode(s))
