# Copyright 20-Sep-2018 ºDeme
# GNU General Public License - V3 <http://www.gnu.org/licenses/>

## ** NOTE: This module is only for documentation. **
##
## Examples of use of json module:
##
## .. code-block:: Nim
##    import json
##
##    # JsonNode to string
##    doAssert ($ js) == "{\"a\":123,\"b\":{\"1\":\"one\"}}"
##
##    # JsonNode from string
##    var js = " [-123.56, true, [],  [\"azf\", false], 56]  ".parseJson
##
##    # Geting values from JsonNode
##    doAssert " true ".parseJson.getBool
##    doAssert " 12 ".parseJson.getInt == 12
##    doAssert " -12.11e22 ".parseJson.getFloat == -12.11e22
##    doAssert "  \"a\\u0030\" ".parseJson.getStr == "a0"
##
##    js = " [-123.56, true, [],  [\"azf\", false], 56]  ".parseJson
##    doAssert js[0].getFloat == -123.56
##    doAssert js[1].getBool
##    var js1 = js[2]
##    doAssert js1.getElems.len == 0
##    js1 = js[3]
##    doAssert js1[0].getStr == "azf"
##    doAssert (not js1[1].getBool)
##    doAssert js[4].getInt == 56
##
##    js = " {\"a\":true, \"a\":123, \"b\": {\"1\": \"one\"} } ".parseJson
##    doAssert 123 == js["a"].getInt
##    js1 = js["b"]
##    doAssert "one" == js1["1"].getStr
##
##    # Writing values to JsonNode
##    js = % true
##    js = % 12
##    js = % -12.11e22
##    js = % "a\\u0030\"
##
##    js = %* [-16, 1.0, "caf", "a\n\tzzð"]
##
##    js = %* {"name": "Isaac", "books": ["Robot Dreams"]}
##    var j = %*
##      [
##        {
##          "name": hisName,
##          "age": 30
##        },
##        {
##          "name": "Susan",
##          "age": herAge
##        }
##      ]
